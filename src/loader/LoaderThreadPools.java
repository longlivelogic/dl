package loader;

import java.io.File;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import util.QueueHelper;
import util.ThreadPool;
import workers.DataFileReader;
import workers.ExtractorWorker;
import workers.KVStoreRowWriter;
import data.ExtractedRow;

/**
 * A helper class that manages all thread pools used by the Loader application.
 * 
 * The Loader application uses three thread pools:
 * readerPool, reading data files, disk I/O intensive
 * extractorPool, extracting rows, CPU intensive
 * writerPool, parsing and writing rows to KVStore, CPU and network I/O
 * intensive
 */
public class LoaderThreadPools {

	/**
	 * Thread pool for reading files. One thread per reader.
	 */
	private ThreadPool readerPool;

	/**
	 * Thread pool for extractors. Dynamically managed and created by readers.
	 */
	private ThreadPool extractorPool;

	/**
	 * Thread pool for parsing/writing KVStore rows. User configured, fixed.
	 */
	private ThreadPool writerPool;

	/**
	 * Application configuration.
	 */
	private LoaderConfig config;

	/**
	 * Creates the application thread pool.
	 * 
	 * @param config
	 *            the application configuration
	 */
	public LoaderThreadPools(LoaderConfig config) {
		readerPool = new ThreadPool(config.getReaderCount());
		extractorPool = new ThreadPool();
		writerPool = new ThreadPool(config.getWriterCount());
		this.config = config;
	}

	/**
	 * Creates background readers and writers and starts them.
	 * 
	 * @param queue
	 *            the row staging queue used to feed writers
	 */
	public void startWorkers(ArrayBlockingQueue<ExtractedRow> queue) {
		startReaders(queue);
		startWriters(queue);
	}

	/**
	 * Starts data file readers.
	 * 
	 * @param queue
	 *            the row staging queue
	 */
	private void startReaders(ArrayBlockingQueue<ExtractedRow> queue) {
		Set<File>[] readerSources = config.getReaderSources();
		for (int i = 0; i < readerSources.length; i++) {
			readerPool.submit(new DataFileReader(readerSources[i], queue, i));
		}
	}

	/**
	 * Starts row writers.
	 * 
	 * @param queue
	 *            the row staging queue
	 */
	private void startWriters(ArrayBlockingQueue<ExtractedRow> queue) {
		for (int i = 0; i < config.getWriterCount(); i++) {
			writerPool.submit(new KVStoreRowWriter(queue, config.getBatchSize()));
		}
	}

	/**
	 * Submits an ExtractorWorker for execution.
	 * 
	 * @param w
	 *            worker
	 */
	public void submit(ExtractorWorker w) {
		extractorPool.submit(w);
	}

	/**
	 * Waits for all submitted tasks to terminate and all rows offered to the
	 * queue to be processed. Blocks the current thread until all threads are
	 * terminated.
	 * 
	 * @param queue
	 *            the queue used to stage rows
	 */
	public void waitForTermination(ArrayBlockingQueue<ExtractedRow> queue) {
		readerPool.waitForTasksAndShutdown();
		extractorPool.waitForTasksAndShutdown();

		notifyEndOfQueue(queue);
		writerPool.waitForTasksAndShutdown();
	}

	/**
	 * Notifies all writers that no more rows are available.
	 * 
	 * @param queue
	 *            the queue used to stage rows
	 */
	private void notifyEndOfQueue(ArrayBlockingQueue<ExtractedRow> queue) {
		for (int i = 0; i < config.getWriterCount(); i++) {
			QueueHelper.putWithRetry(ExtractedRow.END_OF_QUEUE, queue);
		}
	}

}
