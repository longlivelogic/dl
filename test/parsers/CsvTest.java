package parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;

import kvstore.KVStoreHelper;
import oracle.kv.table.Row;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import data.ExtractedRow;
import data.TableDataFile;

public class CsvTest {

	private TableDataFile datafile;

	private InputStream in;

	private ArrayBlockingQueue<ExtractedRow> queue;

	private CsvRowExtractor extractor;

	@Before
	public void setUp() throws Exception {
		KVStoreHelper.getInstance().init("kvstore", "localhost:5000");
		File file = new File("TestTable.5.csv");
		datafile = new TableDataFile(file.toPath());
		in = new FileInputStream(file);
		queue = new ArrayBlockingQueue<>(100);
		extractor = new CsvRowExtractor(datafile, queue);
	}

	@After
	public void tearDown() throws Exception {
		in.close();
	}

	@Ignore
	@Test
	public void testExtractor() throws Exception {
		extractor.extractRows(in);
		Assert.assertEquals(5, queue.size());
	}

	@Ignore
	@Test
	public void testConverter() throws Exception {
		extractor.extractRows(in);
		int i = 1;
		for (ExtractedRow row : queue) {
			Row kvRow = row.convertToKVStoreRow();
			Assert.assertEquals(i, kvRow.get("id").asInteger().get());
			i++;
		}
	}
}
