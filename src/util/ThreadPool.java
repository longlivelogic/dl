package util;

import java.util.Queue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * A thread pool for data loader.
 */
public class ThreadPool {

	private final ExecutorService executor;

	private Queue<Future<?>> submittedTasks;

	/**
	 * Creates a thread pool which dynamically manages its thread according to
	 * loads.
	 */
	public ThreadPool() {
		executor = Executors.newCachedThreadPool();
		submittedTasks = new ConcurrentLinkedQueue<>();
	}

	/**
	 * Creates a thread pool with fixed given number of threads.
	 * 
	 * @param numOfThreads
	 *            the number of threads
	 */
	public ThreadPool(int numOfThreads) {
		executor = Executors.newFixedThreadPool(numOfThreads);
		submittedTasks = new ConcurrentLinkedQueue<>();
	}

	/**
	 * Submits a task for execution.
	 * 
	 * @param task
	 *            the task
	 */
	public void submit(Runnable task) {
		submittedTasks.offer(executor.submit(task));
	}

	/**
	 * Waits for all tasks to complete and then initiates an orderly shutdown of
	 * the thread pool. Blocks the current
	 * thread until the thread pool has been shutdown.
	 */
	public void waitForTasksAndShutdown() {
		executor.shutdown(); // prevents submissions of new tasks
		waitForTasks();
		waitForExecutor();
	}

	/**
	 * Waits for all tasks to terminate.
	 */
	private void waitForTasks() {
		while (!submittedTasks.isEmpty()) {
			Future<?> task = submittedTasks.poll();
			waitForTask(task);
		}
	}

	/**
	 * Waits for the given task to terminate.
	 * 
	 * @param task
	 *            the task
	 */
	private void waitForTask(Future<?> task) {
		boolean running = true;
		while (running) {
			try {
				task.get();
				running = false;
			} catch (InterruptedException e) {
				running = true;
			} catch (ExecutionException | CancellationException e) {
				running = false;
			}
		}
	}

	/**
	 * Wait for the executor to terminate.
	 */
	private void waitForExecutor() {
		boolean running = true;
		while (running) {
			try {
				running = !executor.awaitTermination(1, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				running = true;
			}
		}
	}

}
