package data;

import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kvstore.KVStoreHelper;
import oracle.kv.table.Table;

/**
 * A data structure for managing information related to a data file which can be
 * imported into a KVStore table.
 */
public class TableDataFile implements Comparable<TableDataFile> {

	/**
	 * The data file name pattern. e.g. parent.child.nnn.json
	 */
	private static final Pattern FILE_PATTERN = Pattern.compile(
			"^(.+?)(\\.\\d+)?\\.(csv|xml|json)$", Pattern.CASE_INSENSITIVE);

	/**
	 * The data file.
	 */
	private final Path file;

	/**
	 * The table name.
	 */
	private final String tableName;

	/**
	 * The table. lazy evaluated.
	 */
	private Table table;

	/**
	 * @param file
	 *            the data file
	 */
	public TableDataFile(Path file) {
		this.file = file;
		this.tableName = computeTableName();
	}

	/**
	 * @return the file
	 */
	public Path getFile() {
		return file;
	}

	/**
	 * @return the file name
	 */
	public String getFileName() {
		return file.toString();
	}

	/**
	 * @return the table name
	 */
	public String getTableFullName() {
		return tableName;
	}

	/**
	 * Determines the table name for this data file.
	 */
	private String computeTableName() {
		String fileName = file.getFileName().toString();
		Matcher matcher = FILE_PATTERN.matcher(fileName);
		if (matcher.matches()) {
			return matcher.group(1);
		} else {
			throw new IllegalArgumentException("Cannot determine table name. File name must be: "
					+ FILE_PATTERN.pattern());
		}
	}

	/**
	 * @return the table
	 */
	public Table getTable() {
		if (table == null) {
			table = retrieveTable();
		}
		return table;
	}

	/**
	 * Retrieve the table from the KVStore.
	 */
	private Table retrieveTable() {
		return KVStoreHelper.getInstance().getTable(getTableFullName());
	}

	@Override
	public int compareTo(TableDataFile o) {
		if (o == null) {
			return -1; // null is always bigger
		}
		return getTableFullName().compareTo(o.getTableFullName());
	}

}
