package kvstore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.kv.table.FieldDef.Type;
import oracle.kv.table.FieldValue;
import oracle.kv.table.Row;
import oracle.kv.table.Table;
import data.RowShardKey;

/**
 * TableRowHelper is a helper class which can be used to convert string values
 * into values of correct types for kvstore fields.
 */
public class TableRowHelper {

	/**
	 * The KVStore table.
	 */
	private final Table table;

	/**
	 * Table's full name.
	 */
	private final String fullName;

	/**
	 * The field names of primary keys
	 */
	private final List<String> primaryFields;

	/**
	 * The field names in declaration order.
	 */
	private final List<String> fields;

	/**
	 * The table's shard key, if different from its primary key.
	 */
	private final List<String> shardKey;

	/**
	 * A map between field names and field types.
	 */
	private final Map<String, Type> fieldTypes;

	/**
	 * Constructor.
	 * 
	 * @param table
	 *            the table to whose fields values are converted
	 */
	public TableRowHelper(Table table) {
		this.table = table;
		fullName = table.getFullName();

		fields = table.getFields();
		shardKey = getShardKeyIfNotEqualPrimaryKey(table);
		primaryFields = table.getPrimaryKey();
		fieldTypes = new HashMap<>(fields.size());
		for (String f : fields) {
			fieldTypes.put(f, table.getField(f).getType());
		}
	}

	private List<String> getShardKeyIfNotEqualPrimaryKey(Table t) {
		List<String> primaryKey = t.getPrimaryKey();
		List<String> shardKey = t.getShardKey();
		return shardKey.size() != primaryKey.size() ? shardKey : null;
	}

	/**
	 * @return the field names in declaration order
	 */
	public List<String> getFields() {
		return fields;
	}

	/**
	 * @return the primary field names
	 */
	public List<String> getPrimaryFields() {
		return primaryFields;
	}

	/**
	 * Creates an empty table row.
	 * 
	 * @return an empty Table row
	 */
	public Row createEmptyRow() {
		return table.createRow();
	}

	/**
	 * Creates a table row from a json string.
	 * 
	 * @param json
	 *            the json string
	 * @return a table row
	 */
	public Row createRowFromJson(String json) {
		return table.createRowFromJson(json, false);
	}

	/**
	 * Puts a string value into the given field of the given row.
	 * 
	 * @param fieldName
	 *            the field name
	 * @param val
	 *            the value in string
	 * @param row
	 *            the row the value to be put into
	 */
	public void putValue(String fieldName, String val, Row row) {
		Type type = fieldTypes.get(fieldName);
		if (type == null) {
			return; // skip non-existing fields
		}

		if ((val == null || val.trim().isEmpty()) && type != Type.STRING) {
			row.putNull(fieldName);
			return;
		}

		switch (type) {
		case BOOLEAN:
			row.put(fieldName, Boolean.parseBoolean(val));
			break;
		case DOUBLE:
			row.put(fieldName, Double.parseDouble(val));
			break;
		case FLOAT:
			row.put(fieldName, Float.parseFloat(val));
			break;
		case INTEGER:
			row.put(fieldName, Integer.parseInt(val));
			break;
		case LONG:
			row.put(fieldName, Long.parseLong(val));
			break;
		case ENUM:
			row.putEnum(fieldName, val);
			break;
		case STRING:
			row.put(fieldName, val);
			break;
		default:
			throw new IllegalArgumentException("Unsupported type: " + type);
		}
	}

	/**
	 * Creates the given row's shard key fields, if the table's shard key is
	 * different from its primary key. Otherwise, returns null.
	 * 
	 * @param row
	 *            the row
	 * @return a RowShardKey
	 */
	public RowShardKey getShardKey(Row row) {
		if (shardKey == null) {
			return null;
		}

		return new RowShardKey(fullName, getShardFields(row));
	}

	/**
	 * Extracts the fields values of the shard key fields, in the same order.
	 * 
	 * @param row
	 *            the row
	 * @return shard key field values
	 */
	private List<FieldValue> getShardFields(Row row) {
		List<FieldValue> shardFields = new ArrayList<>(shardKey.size());

		for (String key : shardKey) {
			shardFields.add(row.get(key));
		}

		return shardFields;
	}
}
