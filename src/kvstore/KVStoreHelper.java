package kvstore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import loader.LoaderConfig;
import oracle.kv.Durability;
import oracle.kv.Durability.ReplicaAckPolicy;
import oracle.kv.Durability.SyncPolicy;
import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.OperationExecutionException;
import oracle.kv.table.Row;
import oracle.kv.table.Table;
import oracle.kv.table.TableAPI;
import oracle.kv.table.TableOperation;
import oracle.kv.table.TableOperationFactory;
import oracle.kv.table.TableOperationResult;

/**
 * KVStoreHelper provides one-stop access to KVStores.
 * 
 * For simplicity, this helper is currently a singleton and can support one
 * store only. However, with some proper factory methods, there is no inherit
 * limit on the number of stores this class can support.
 * 
 * Multi-threading:
 * Designed to be thread safe and can be shared with multiple threads.
 */
public final class KVStoreHelper {

	/**
	 * The singleton instance.
	 */
	private static final KVStoreHelper INSTANCE = new KVStoreHelper();

	/**
	 * Returns an instance of this class.
	 * 
	 * @return a {@link KVStoreHelper}
	 */
	public static KVStoreHelper getInstance() {
		return INSTANCE;
	}

	/**
	 * The {@link KVStoreConfig} used to create {@link KVStore} handles.
	 */
	private KVStoreConfig config;

	/**
	 * A set of all opened store handles.
	 */
	private Set<KVStore> openStores;

	/**
	 * The thread local {@link TableAPI} handle.
	 */
	private ThreadLocal<TableAPI> api;

	/**
	 * Hide constructor.
	 */
	private KVStoreHelper() {
	}

	/**
	 * Initializes this helper.
	 * 
	 * @param config
	 *            the application runtime configuration
	 */
	public void init(LoaderConfig config) {
		init(config.getStoreName(), config.getHosts());
	}

	/**
	 * Initializes this helper.
	 * 
	 * @param storeName
	 *            the kvstore name
	 * @param hosts
	 *            the kvstore hosts
	 */
	public void init(String storeName, String... hosts) {
		this.config = new KVStoreConfig(storeName, hosts);
		Durability dur = new Durability(SyncPolicy.NO_SYNC, SyncPolicy.NO_SYNC,
				ReplicaAckPolicy.NONE);
		this.config.setDurability(dur);
		this.openStores = new HashSet<>();
		this.api = new ThreadLocal<>();
	}

	/**
	 * Close all opened resourced owned by this helper.
	 */
	public void close() {
		for (KVStore kvStore : openStores) {
			kvStore.close();
		}
	}

	/**
	 * Retrieves the table using its name.
	 * 
	 * @param tableName
	 *            the table name
	 * @return a table handle
	 */
	public Table getTable(String tableName) {
		TableAPI api = getApi();
		String[] namePath = tableName.split("\\.");

		Table t = api.getTable(namePath[0]);
		for (int i = 1; i < namePath.length; i++) {
			if (t == null) {
				return null;
			}
			t = t.getChildTable(namePath[i]);
		}

		return t;
	}

	/**
	 * Stores the row into KVStore.
	 * 
	 * @param row
	 *            the row to be put
	 */
	public void putRow(Row row) {
		getApi().put(row, null, null);
	}

	/**
	 * Stores the list of rows into KVStore. The rows must have the same shard
	 * key.
	 * 
	 * @param rows
	 *            the rows to be put
	 * @return a list of indices of failed operations
	 */
	public List<Integer> putRows(List<Row> rows) {
		List<TableOperation> operations = createBatchOperations(rows);
		return executeBatch(operations);
	}

	/**
	 * Creates the batch table operations for the list of rows to be put.
	 * 
	 * @param rows
	 *            the rows
	 * @return a list of operations
	 */
	private List<TableOperation> createBatchOperations(List<Row> rows) {
		List<TableOperation> operations = new ArrayList<>(rows.size());
		TableOperationFactory factory = getApi().getTableOperationFactory();

		for (Row r : rows) {
			operations.add(factory.createPut(r, null, false));
		}
		return operations;
	}

	/**
	 * Executes the batch operations, and returns a list of failed rows.
	 * 
	 * @param batchOps
	 *            the operations
	 * @return the indices of failed operations
	 */
	private List<Integer> executeBatch(List<TableOperation> batchOps) {
		try {
			List<TableOperationResult> opResult = getApi().execute(batchOps, null);

			List<Integer> failedRows = new ArrayList<>();
			int i = 0;
			for (TableOperationResult res : opResult) {
				if (!res.getSuccess()) {
					failedRows.add(i);
				}
				i++;
			}
			return failedRows;
		} catch (OperationExecutionException e) {
			return Collections.emptyList();
		}
	}

	/**
	 * @return a TableAPI handle
	 */
	private TableAPI getApi() {
		if (api.get() == null) {
			KVStore store = KVStoreFactory.getStore(config);
			openStores.add(store);
			api.set(store.getTableAPI());
		}
		return api.get();
	}

}
