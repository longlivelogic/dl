package parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import log.RejectedRowLogger;
import oracle.kv.table.FieldDef.Type;
import data.ExtractedRow;
import data.TableDataFile;

/**
 * JsonRowExtractor is used to parse json data files.
 * 
 * Only parses top-level json objects.
 * 
 */
public class JsonRowExtractor extends TextRowExtractor {

	// Buffer to store a record's original string
	private final StringBuffer recordBufferString = new StringBuffer(1000);

	/** Extractor's status fileds **/
	private short bracketLevel = 0;
	private boolean isInSingleQuote;
	private boolean isInDoubleQuote;

	/**
	 * The converter used to convert rows extracted by this extractor.
	 */
	private JsonRowConverter converter;

	public JsonRowExtractor(TableDataFile file, ArrayBlockingQueue<ExtractedRow> queue) {
		super(file, queue);
	}

	@Override
	protected void extractTextRows(BufferedReader r) throws IOException {
		converter = new JsonRowConverter(getTable());
		String line = null;
		while ((line = r.readLine()) != null) {
			parseLine(line, true);
		}
	}

	private void parseLine(String line, boolean isAddingToQueue) {
		for (int i = 0; i < line.length(); i++) {
			char currentChar = line.charAt(i);
			switch (currentChar) {
			case '{':
				if (!isInSingleQuote && !isInDoubleQuote) {
					bracketLevel++;
					if (bracketLevel == 1)
						recordBufferString.setLength(0);// reset buffer
				}
				break;
			case '}':
				if (!isInSingleQuote && !isInDoubleQuote) {
					bracketLevel--;
					if (bracketLevel == 0) {
						recordBufferString.append(currentChar);
						if (isAddingToQueue) {
							// finish one record
							addRow(recordBufferString.toString());
						} else {
							aggregateTypes(recordBufferString.toString());
						}
						isInSingleQuote = false;
						isInDoubleQuote = false;
					}
				}
				break;
			case '\'':
				if (!isInDoubleQuote)
					isInSingleQuote = !isInSingleQuote;
				break;
			case '\"':
				if (!isInSingleQuote)
					isInDoubleQuote = !isInDoubleQuote;
				break;
			default:
				break;
			}
			recordBufferString.append(currentChar);
		}
	}

	/**
	 * Creates an extracted row for the line and add it to the queue.
	 * 
	 * @param keyValuePairs
	 *            the field names and values
	 * @param record
	 *            the line text
	 */
	private void addRow(String record) {
		putRow(new ExtractedRow(record, record, getFileName(), converter));
	}

	/**
	 * Only used for extractSchemas
	 */
	private Map<String, Type> types;
	private ScriptEngine engine;

	/**
	 * Aggregate the types of fields by determine the types of the row values
	 * 
	 * @param jsonRecord
	 */
	private void aggregateTypes(String jsonRecord) {
		if (types == null || engine == null)
			return;

		Map<String, String> keyvalues = new HashMap<String, String>();
		try {
			engine.eval(
			// func to enumerate fields
			"var getEnumPropertyNames = function (obj) {\n" + "	var props = [];\n"
					+ "	for (prop in obj) {\n" + "		props.push(prop);\n" + "	}\n"
					+ "return props;\n"
					+ "}\n"
					+
					// json obj
					"var data = " + jsonRecord + ";\n"
					+ "var properties = getEnumPropertyNames(data);\n"
					+ "var totalLength = properties.length;\n");
			Double propertiesLength = Double.parseDouble(engine.get("totalLength").toString());
			int totalLength = propertiesLength.intValue();

			for (int i = 0; i < totalLength; i++) {
				engine.eval("var fieldName = properties[" + i + "];");
				String fieldName = engine.get("fieldName").toString();
				engine.eval("var data = " + jsonRecord + ";\n var fieldValue = data." + fieldName
						+ ";");
				String fieldValue = engine.get("fieldValue") != null ? engine.get("fieldValue")
						.toString() : null;
				keyvalues.put(fieldName, fieldValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
			;
		}

		for (String key : keyvalues.keySet()) {
			// explore each key-value pairs to determine the type
			String value = keyvalues.get(key);
			Type currentValueType = Type.STRING;
			if (value == null) {
				// null value
			} else if (value.matches("true|false")) {
				currentValueType = Type.BOOLEAN;
			} else if (value.matches("[0-9]+")) {
				currentValueType = Type.INTEGER;
			} else if (value.matches("[0-9]+\\.[0-9]+")) {
				currentValueType = Type.DOUBLE;
			}
			Type type = types.get(key);
			if (type == null || currentValueType == Type.STRING) {
				types.put(key, currentValueType);
			} else if (type == Type.STRING) {
				types.put(key, Type.STRING);
			} else if (currentValueType == Type.DOUBLE) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
				else
					types.put(key, Type.DOUBLE);
			} else if (currentValueType == Type.INTEGER) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
			} else if (currentValueType == Type.BOOLEAN) {
				if (type == Type.INTEGER || type == Type.DOUBLE)
					types.put(key, Type.STRING);
			}
		}
	}

	@Override
	public Map<String, Type> extractSchemas(InputStream in) {
		ScriptEngineManager manager = new ScriptEngineManager();
		engine = manager.getEngineByName("js");
		types = new HashMap<String, Type>();
		String line = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {
			while ((line = reader.readLine()) != null) {
				parseLine(line, false);
			}
		} catch (IOException e) {
			RejectedRowLogger.log(line, getFileName());
		}
		return types;
	}

}
