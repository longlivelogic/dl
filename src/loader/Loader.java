package loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;

import kvstore.KVStoreHelper;
import data.ExtractedRow;

/**
 * Application's main class.
 */
public class Loader {

	/**
	 * The name of the configuration file.
	 */
	private static final String CONFIG_FILE = "loader.properties";

	/**
	 * The application's runtime configuration.
	 */
	private static LoaderConfig config;

	/**
	 * The thread pools used by this application.
	 */
	private static LoaderThreadPools threadPools;

	/**
	 * The row staging buffer between parsers and writers.
	 */
	private static ArrayBlockingQueue<ExtractedRow> rowQueue;

	/**
	 * @return the thread pools
	 */
	public static LoaderThreadPools getThreadPools() {
		return threadPools;
	}

	/**
	 * @return the application's runtime configuration
	 */
	public static LoaderConfig getConfig() {
		return config;
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		loadConfig(CONFIG_FILE);

		if (args != null && args.length > 0) {
			for (String arg : args) {
				if ("-s".equals(arg) || "--schema".equals(arg)) {
					// dispatch to schema generator
					SchemaGenerator.generateSchemas();
					return;
				}
			}
		}

		initKVStore();
		createThreadPools();
		createBufferQueue();

		startReadersAndWriters();

		waitForTermination();
	}

	/**
	 * Loads the application's runtime configuration.
	 * 
	 * @param configFile
	 *            the configuration file name
	 */
	private static void loadConfig(String configFile) {
		Properties props = loadConfigFile(CONFIG_FILE);
		if (props == null) {
			System.exit(1);
		}

		config = new LoaderConfig(props);
	}

	/**
	 * Loads the configuration file, and returns the loaded Properties.
	 * 
	 * @param configFile
	 *            the configuration file name
	 * @return the loaded Properties
	 */
	private static Properties loadConfigFile(String configFile) {
		try (Reader r = new BufferedReader(new FileReader(configFile))) {
			Properties p = new Properties(getDefaultConfig());
			p.load(r);
			return p;
		} catch (FileNotFoundException e) {
			System.err.println("Missing configuration file: " + configFile);
		} catch (IOException e) {
			System.err.println("Cannot load configuration file '" + CONFIG_FILE + "': "
					+ e.getMessage());
		}

		return null;
	}

	/**
	 * Returns the loader's default configuration.
	 * 
	 * @return the default configuration
	 */
	private static Properties getDefaultConfig() {
		Properties p = new Properties();
		p.setProperty(LoaderConfig.WRITER_COUNT_KEY, "3");
		p.setProperty(LoaderConfig.WRITER_BATCH_SIZE_KEY, "1000");
		return p;
	}

	/**
	 * Initializes the KVStoreHelper.
	 */
	private static void initKVStore() {
		KVStoreHelper.getInstance().init(config);
	}

	/**
	 * Creates the application's thread pools.
	 */
	private static void createThreadPools() {
		threadPools = new LoaderThreadPools(config);
	}

	/**
	 * Creates the buffered queue between parsers and writers for staging parsed
	 * rows.
	 */
	private static void createBufferQueue() {
		// this is an initial wild guess, could be tuned
		final int bufferSizePerWriter = 100;
		rowQueue = new ArrayBlockingQueue<>(config.getWriterCount() * bufferSizePerWriter, false);
	}

	/**
	 * Starts background readers and writers.
	 */
	private static void startReadersAndWriters() {
		threadPools.startWorkers(rowQueue);
	}

	/**
	 * Waits for all workers to terminate.
	 */
	private static void waitForTermination() {
		threadPools.waitForTermination(rowQueue);

		KVStoreHelper.getInstance().close();
	}

}
