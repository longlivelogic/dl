package data;

import oracle.kv.table.Row;
import parsers.ParseException;
import parsers.RowConverter;

/**
 * The data structure representing some content extracted from a data file which
 * can be further converted into a KVStore row.
 */
public class ExtractedRow {

	/**
	 * A special row which is used to mark the end of a staging queue. This is
	 * used to notify row writers that no more rows are available for import.
	 */
	public static final ExtractedRow END_OF_QUEUE = new ExtractedRow(null, null, null, null);

	/**
	 * The extracted content representing a complete row.
	 */
	private final Object content;

	/**
	 * The original string representing this row. Used for logging
	 * purposes.
	 */
	private final String originalText;

	/**
	 * The name of the data file. Used for logging purposes.
	 */
	private final String fileName;

	/**
	 * The row converter for creating KVStore table rows.
	 */
	private final RowConverter converter;

	/**
	 * The converted kvstore row. lazy evaluated
	 */
	private Row kvRow;

	/**
	 * Creates a new ExtractedRow.
	 * 
	 * @param content
	 *            the row's content
	 * @param originalText
	 *            the original text for this row
	 * @param fileName
	 *            the name of the data file containing the row's text
	 * @param converter
	 *            the row converter used to convert this into KVStore rows
	 */
	public ExtractedRow(Object content, String originalText, String fileName, RowConverter converter) {
		this.content = content;
		this.originalText = originalText;
		this.fileName = fileName;
		this.converter = converter;
	}

	/**
	 * @return the data file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the original text for this row
	 */
	public String getOriginalText() {
		return originalText;
	}

	/**
	 * @return the KVStore row converted from this extracted row
	 * @throws ParseException
	 *             if the row cannot be converted
	 */
	public Row convertToKVStoreRow() throws ParseException {
		if (kvRow == null && content != null) {
			kvRow = converter.convert(content);
		}
		return kvRow;
	}

}
