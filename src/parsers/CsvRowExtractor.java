package parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import kvstore.TableRowHelper;
import log.RejectedRowLogger;
import oracle.kv.table.FieldDef.Type;
import au.com.bytecode.opencsv.CSVParser;
import data.ExtractedRow;
import data.TableDataFile;

/**
 * CsvRowExtractor is used to parse csv data files.
 * 
 * The first line of the file is assumed to be the header. If the first line is
 * empty, then the file is assume to have no header, and fields are assumed to
 * be in the same order as declared on the table.
 * 
 * Do not support multi-line rows.
 */
public class CsvRowExtractor extends TextRowExtractor {

	/**
	 * The csv parser.
	 */
	private CSVParser parser;

	/**
	 * Table data file.
	 */
	private TableDataFile file;

	/**
	 * The converter used to convert rows extracted by this extractor.
	 */
	private MapRowConverter converter;

	/**
	 * Only used for extractSchemas
	 */
	private Map<String, Type> types;

	public CsvRowExtractor(TableDataFile file, ArrayBlockingQueue<ExtractedRow> queue) {
		super(file, queue);
		parser = new CSVParser();
		this.file = file;
	}

	@Override
	protected void extractTextRows(BufferedReader r) throws IOException {
		converter = new MapRowConverter(file);
		String[] fieldNames = getHeaderNames(r);

		String line;
		while ((line = readNextLine(r)) != null) {
			addRow(fieldNames, line);
		}
	}

	/**
	 * Returns the header names if this csv has a valid header. Returns the
	 * default table field names, otherwise.
	 * 
	 * @param r
	 *            the reader
	 * @return a list of header names
	 * @throws IOException
	 *             on I/O error
	 */
	private String[] getHeaderNames(BufferedReader r) throws IOException {
		String headLine = r.readLine();
		if (headLine != null && !headLine.trim().isEmpty()) {
			return parseHeader(headLine.trim());
		}

		return getDefaultHeaderNames();
	}

	/**
	 * Parses the headline into a list of field names. Falls back to default
	 * fields if the headline cannot be parsed.
	 * 
	 * @param headLine
	 *            the header line
	 * @return a list of field names
	 */
	private String[] parseHeader(String headLine) {
		try {
			return parser.parseLine(headLine);
		} catch (IOException e) {
			return getDefaultHeaderNames();
		}
	}

	/**
	 * @return the declared table fields
	 */
	private String[] getDefaultHeaderNames() {
		TableRowHelper helper = new TableRowHelper(getTable());
		return helper.getFields().toArray(new String[0]);
	}

	/**
	 * Reads the next non-empty line. Returns null on EOF.
	 * 
	 * @param r
	 *            the reader
	 * @return the next non-empty line or null on EOF
	 * @throws IOException
	 *             on I/O error
	 */
	private String readNextLine(BufferedReader r) throws IOException {
		String line;
		while ((line = r.readLine()) != null) {
			if (!line.trim().isEmpty()) {
				return line.trim();
			}
		}
		return null;
	}

	/**
	 * Creates an extracted row for the line and add it to the queue.
	 * 
	 * @param fieldNames
	 *            the field names
	 * @param line
	 *            the line text
	 */
	private void addRow(String[] fieldNames, String line) {
		try {
			String[] fieldValues = parser.parseLine(line);
			Map<String, String> rowContent = createRowContent(fieldNames, fieldValues);
			putRow(new ExtractedRow(rowContent, line, getFileName(), converter));
		} catch (IOException e) {
			RejectedRowLogger.log(line, getFileName());
		}
	}

	/**
	 * Creates the content for the extracted row given its field names and
	 * corresponding values.
	 * 
	 * @param fieldNames
	 *            the field names
	 * @param fieldValues
	 *            the field values
	 * @return the extracted row content
	 */
	private Map<String, String> createRowContent(String[] fieldNames, String[] fieldValues) {
		Map<String, String> rowContent = new HashMap<>(fieldNames.length);
		for (int i = 0; i < fieldNames.length; i++) {
			if (i < fieldValues.length) {
				rowContent.put(fieldNames[i], fieldValues[i]);
			} else {
				rowContent.put(fieldNames[i], null);
			}
		}
		return rowContent;
	}

	@Override
	public Map<String, Type> extractSchemas(InputStream in) {
		String line = null;
		types = new HashMap<String, Type>();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {
			String[] fieldNames = getHeaderNames(reader);
			while ((line = readNextLine(reader)) != null) {
				String[] fieldValues = parser.parseLine(line);
				Map<String, String> rowContent = createRowContent(fieldNames, fieldValues);
				aggregateTypes(rowContent);
			}
		} catch (IOException e) {
			RejectedRowLogger.log(line, getFileName());
		}
		return types;
	}

	/**
	 * Aggregate the types of fields by determine the types of the row values
	 * 
	 * @param keyvalues
	 */
	private void aggregateTypes(Map<String, String> keyvalues) {
		if (types == null)
			return;

		for (String key : keyvalues.keySet()) {
			// explore each key-value pairs to determine the type
			String value = keyvalues.get(key);
			Type currentValueType = Type.STRING;
			if (value == null) {
				// null value
			} else if (value.matches("true|false")) {
				currentValueType = Type.BOOLEAN;
			} else if (value.matches("[0-9]+")) {
				currentValueType = Type.INTEGER;
			} else if (value.matches("[0-9]+\\.[0-9]+")) {
				currentValueType = Type.DOUBLE;
			}
			Type type = types.get(key);
			if (type == null || currentValueType == Type.STRING) {
				types.put(key, currentValueType);
			} else if (type == Type.STRING) {
				types.put(key, Type.STRING);
			} else if (currentValueType == Type.DOUBLE) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
				else
					types.put(key, Type.DOUBLE);
			} else if (currentValueType == Type.INTEGER) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
			} else if (currentValueType == Type.BOOLEAN) {
				if (type == Type.INTEGER || type == Type.DOUBLE)
					types.put(key, Type.STRING);
			}
		}
	}

}
