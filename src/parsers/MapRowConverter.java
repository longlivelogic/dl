package parsers;

import java.util.Map;

import kvstore.TableRowHelper;
import loader.Loader;
import loader.LoaderConfig;
import oracle.kv.table.Row;
import data.TableDataFile;

/**
 * MapRowConverter can convert a Map<String, String> into a KVStore Row.
 */
public class MapRowConverter extends AbstractRowConverter {

	/**
	 * The helper for creating table rows.
	 */
	private TableRowHelper helper;

	private boolean isAutoFillPrimaryKeys;

	/**
	 * @param file
	 *            the table data file
	 */
	public MapRowConverter(TableDataFile file) {
		super(file);
		helper = new TableRowHelper(file.getTable());

		LoaderConfig config = Loader.getConfig();
		isAutoFillPrimaryKeys = config == null ? false : config.isAutoFillPrimaryKey();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Row convert(Object rowContent) throws ParseException {
		try {
			return convertFromMap((Map<String, String>) rowContent);
		} catch (IllegalArgumentException e) {
			throw new ParseException(e.getMessage());
		}
	}

	private Row convertFromMap(Map<String, String> content) {
		Row row = helper.createEmptyRow();

		for (String fieldName : helper.getFields()) {
			String fieldValue = content.get(fieldName);
			if (fieldValue != null)
				helper.putValue(fieldName, fieldValue, row);
			else {
				if (helper.getPrimaryFields().contains(fieldName)) {
					// prevent the row from being inserted without primary keys
					if (isAutoFillPrimaryKeys) {
						helper.putValue(fieldName, "N/A", row);
					} else {
						return null;
					}
				}
			}
		}

		return row;
	}

}
