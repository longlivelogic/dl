package parsers;

import oracle.kv.table.Row;

/**
 * RowConverter converts ExtractedRows into kvstore table Rows.
 */
public interface RowConverter {

	/**
	 * Creates a KVStore Row by converting an extracted row.
	 * 
	 * A converter is responsible for extracting and converting values for each
	 * field, inserting nulls for missing fields and ignoring fields which do
	 * not exist in the table.
	 * 
	 * @param rowContent
	 *            an extracted row's content
	 * @return a KVStore row
	 * @throws ParseException
	 *             if the row cannot be converted
	 */
	Row convert(Object rowContent) throws ParseException;

}
