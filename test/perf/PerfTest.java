package perf;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ArrayBlockingQueue;

import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.table.Row;
import oracle.kv.table.Table;
import oracle.kv.table.TableAPI;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import parsers.CsvRowExtractor;
import data.ExtractedRow;
import data.TableDataFile;

public class PerfTest {

	private KVStore store;

	private Table table;

	@Before
	public void setUp() throws Exception {
		KVStoreConfig config = new KVStoreConfig("kvstore", "localhost:5000");
		store = KVStoreFactory.getStore(config);
		table = store.getTableAPI().getTable("TestTable");
	}

	@After
	public void tearDown() throws Exception {
		store.close();
	}

	@Test
	public void testPutRow() {
		final int iter = 1000000;
		long start = System.currentTimeMillis();
		for (int i = 0; i < iter; i++) {
			Row r = table.createRow();
			// 40 chars
			r.put("str", "1234567890123456789012345678901234567890");
			r.put("int", 12345);
			r.put("dbl", 123.0);
			r.put("bol", true);
			r.putEnum("enu", "small");
		}
		long end = System.currentTimeMillis();
		System.out.println("Time(put): " + (end - start) + ", iter: " + iter);
	}

	@Test
	public void testPutRowAndConvert() {
		final int iter = 1000000;
		long start = System.currentTimeMillis();
		for (int i = 0; i < iter; i++) {
			Row r = table.createRow();
			// 40 chars
			r.put("str", "1234567890123456789012345678901234567890");
			r.put("int", Integer.parseInt("12345"));
			r.put("dbl", Double.parseDouble("123.00"));
			r.put("bol", Boolean.parseBoolean("true"));
			r.putEnum("enu", "small");
		}
		long end = System.currentTimeMillis();
		System.out.println("Time(put+convert): " + (end - start) + ", iter: " + iter);
	}

	@Test
	public void testQueuePut() throws Exception {
		final int iter = 1000000;
		ArrayBlockingQueue<ExtractedRow> queue = new ArrayBlockingQueue<>(iter * 2);

		long start = System.currentTimeMillis();
		for (int i = 0; i < iter; i++) {
			queue.put(new ExtractedRow(null, "text", "file", null));
		}
		long end = System.currentTimeMillis();
		System.out.println("Time(queue put): " + (end - start) + ", iter: " + queue.size());
	}

	@Test
	public void testParseCSV() throws Exception {
		Path testFile = new File("TestTable.csv").toPath();
		byte[] content = Files.readAllBytes(testFile);

		InputStream in = new ByteArrayInputStream(content);
		TableDataFile tableFile = new TableDataFile(testFile);
		ArrayBlockingQueue<ExtractedRow> queue = new ArrayBlockingQueue<>(200000);

		CsvRowExtractor extractor = new CsvRowExtractor(tableFile, queue);

		long start = System.currentTimeMillis();
		extractor.extractRows(in);
		long end = System.currentTimeMillis();
		System.out.println("Time(extract:100000): " + (end - start));

		start = System.currentTimeMillis();
		while (!queue.isEmpty()) {
			queue.take().convertToKVStoreRow();
		}
		end = System.currentTimeMillis();
		System.out.println("Time(parse:100000): " + (end - start));
	}

	@Test
	public void testStoreRow() {
		final int iter = 100000;
		TableAPI api = store.getTableAPI();
		long start = System.currentTimeMillis();
		for (int i = 0; i < iter; i++) {
			Row r = table.createRow();
			// 40 chars
			r.put("sid", 12345);
			r.put("id", 12345);
			r.put("firstname", "1234567890");
			r.put("lastname", "1234567890");
			r.put("email", "small");
			r.put("text", "small");
			api.put(r, null, null);
		}
		long end = System.currentTimeMillis();
		System.out.println("Time(store): " + (end - start) + ", iter: " + iter);
	}

}
