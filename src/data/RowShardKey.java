package data;

import java.util.List;

import oracle.kv.table.FieldValue;

/**
 * Data structure representing the shard key fields of a KVStore row. Rows with
 * equal RowShardKeys can be inserted in one batch operation.
 */
public class RowShardKey {

	private String fullTableName;

	private List<FieldValue> shardFields;

	/**
	 * Create the RowShardKey for the given row.
	 * 
	 * @param row
	 *            the row
	 */
	public RowShardKey(String fullTableName, List<FieldValue> shardFields) {
		this.fullTableName = fullTableName;
		this.shardFields = shardFields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullTableName == null) ? 0 : fullTableName.hashCode());
		result = prime * result + ((shardFields == null) ? 0 : shardFields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RowShardKey other = (RowShardKey) obj;
		if (fullTableName == null) {
			if (other.fullTableName != null) {
				return false;
			}
		} else if (!fullTableName.equals(other.fullTableName)) {
			return false;
		}
		if (shardFields == null) {
			if (other.shardFields != null) {
				return false;
			}
		} else if (!shardFields.equals(other.shardFields)) {
			return false;
		}
		return true;
	}

}
