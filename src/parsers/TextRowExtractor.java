package parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ArrayBlockingQueue;

import oracle.kv.table.Table;
import util.QueueHelper;
import data.ExtractedRow;
import data.TableDataFile;

/**
 * A base class for all extractors that parse text files.
 */
public abstract class TextRowExtractor implements RowExtractor {

	/**
	 * The data file to be parsed.
	 */
	private final TableDataFile file;

	/**
	 * The queue for staging extracted rows.
	 */
	private final ArrayBlockingQueue<ExtractedRow> queue;

	/**
	 * Constructor.
	 * 
	 * @param file
	 *            the data file
	 * @param queue
	 *            the queue for staging extracted rows
	 */
	protected TextRowExtractor(TableDataFile file, ArrayBlockingQueue<ExtractedRow> queue) {
		this.file = file;
		this.queue = queue;
	}

	/**
	 * @return the data file name
	 */
	protected final String getFileName() {
		return file.getFileName();
	}

	/**
	 * @return the table into which rows are imported
	 */
	protected final Table getTable() {
		return file.getTable();
	}

	/**
	 * Puts the extracted row into staging queue assigned to this extractor.
	 * 
	 * @param row
	 *            the row to be put
	 */
	protected final void putRow(ExtractedRow row) {
		QueueHelper.putWithRetry(row, queue);
	}

	@Override
	public final void extractRows(InputStream in) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {
			extractTextRows(reader);
		}
	}

	/**
	 * Extracts strings representing each row and puts them into the given
	 * queue.
	 * 
	 * @param r
	 *            the input reader
	 * @param textRowQueue
	 *            the queue for staging extracted rows
	 * @throws IOException
	 *             on I/O error
	 */
	abstract protected void extractTextRows(BufferedReader r) throws IOException;

}
