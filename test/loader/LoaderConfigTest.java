/**
 * 
 */
package loader;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * Test the LoaderConfig class.
 */
public class LoaderConfigTest {

	/**
	 * Default valid properties.
	 */
	private Properties p;

	/*
	 * Valid test values.
	 */
	private final String name = "ASGEAQ#GVCEWAVF@#1234Q?><;'";
	private final String hosts = "localhost:5000";
	private final String src = "data";
	private final String writerCount = "3";
	private final String batchSize = "1000";
	private final String tagName = "MOCK_DATA";

	@Before
	public void setUp() {
		p = new Properties();
		p.setProperty(LoaderConfig.STORENAME_KEY, name);
		p.setProperty(LoaderConfig.HOSTS_KEY, hosts);
		p.setProperty(String.format(LoaderConfig.DISK_SRC_KEY, 1), src);
		p.setProperty(LoaderConfig.WRITER_COUNT_KEY, writerCount);
		p.setProperty(LoaderConfig.WRITER_BATCH_SIZE_KEY, batchSize);
		p.setProperty(LoaderConfig.XML_ROW_TAG_NAME_KEY, tagName);
	}

	@Test
	public void testStoreName() {
		LoaderConfig c = new LoaderConfig(p);
		assertEquals(name, c.getStoreName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoStoreName() {
		p.remove(LoaderConfig.STORENAME_KEY);
		new LoaderConfig(p);
	}

	@Test
	public void testHosts() {
		LoaderConfig c = new LoaderConfig(p);
		assertArrayEquals(new String[] { hosts }, c.getHosts());
	}

	@Test
	public void testMultiHosts() {
		String aHost = "another:9000";
		p.setProperty(LoaderConfig.HOSTS_KEY, " " + hosts + " ,  " + aHost + " ");
		LoaderConfig c = new LoaderConfig(p);
		assertArrayEquals(new String[] { hosts, aHost }, c.getHosts());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidHosts() {
		String badHost = "another:99000";
		p.setProperty(LoaderConfig.HOSTS_KEY, hosts + " ,  " + badHost);
		new LoaderConfig(p);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoHosts() {
		p.remove(LoaderConfig.HOSTS_KEY);
		new LoaderConfig(p);
	}

	@Test
	public void testWriterCount() {
		LoaderConfig c = new LoaderConfig(p);
		assertEquals(3, c.getWriterCount());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidWriterCount() {
		String badCount = "abcd";
		p.setProperty(LoaderConfig.WRITER_COUNT_KEY, badCount);
		new LoaderConfig(p);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoWriterCount() {
		p.remove(LoaderConfig.WRITER_COUNT_KEY);
		new LoaderConfig(p);
	}

	@Test
	public void testBatchSize() {
		LoaderConfig c = new LoaderConfig(p);
		assertEquals(1000, c.getBatchSize());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidBatchSize() {
		String badSize = "abcd";
		p.setProperty(LoaderConfig.WRITER_BATCH_SIZE_KEY, badSize);
		new LoaderConfig(p);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoBatchSize() {
		p.remove(LoaderConfig.WRITER_BATCH_SIZE_KEY);
		new LoaderConfig(p);
	}

	@Test
	public void testSrc() throws Exception {
		try {
			createDir(src);
			LoaderConfig c = new LoaderConfig(p);
			Set<File> expected = new HashSet<File>();
			expected.add(new File(src).getCanonicalFile());

			assertEquals(1, c.getReaderCount());
			assertEquals(expected, c.getReaderSources()[0]);
		} finally {
			removeDir(src);
		}
	}

	@Test
	public void testSrcWithTwoReaders() throws Exception {
		String aDir = "another";

		try {
			createDir(src);
			createDir(aDir);
			p.setProperty(String.format(LoaderConfig.DISK_SRC_KEY, 2), aDir);

			LoaderConfig c = new LoaderConfig(p);
			assertEquals(2, c.getReaderCount());

			Set<File> expected = new HashSet<File>();
			expected.add(new File(src).getCanonicalFile());
			assertEquals(expected, c.getReaderSources()[0]);

			expected.clear();
			expected.add(new File(aDir).getCanonicalFile());
			assertEquals(expected, c.getReaderSources()[1]);
		} finally {
			removeDir(src);
			removeDir(aDir);
		}
	}

	@Test
	public void testSrcWithTwoReadersWithDup() throws Exception {
		String aDir = "another";

		try {
			createDir(src);
			createDir(aDir);
			p.setProperty(String.format(LoaderConfig.DISK_SRC_KEY, 2), aDir + "," + src);

			LoaderConfig c = new LoaderConfig(p);
			assertEquals(2, c.getReaderCount());

			Set<File> expected = new HashSet<File>();
			expected.add(new File(src).getCanonicalFile());
			assertEquals(expected, c.getReaderSources()[0]);

			expected.clear();
			expected.add(new File(aDir).getCanonicalFile());
			assertEquals(expected, c.getReaderSources()[1]);
		} finally {
			removeDir(src);
			removeDir(aDir);
		}
	}

	/**
	 * Creates the test directory
	 * 
	 * @param name
	 *            the directory name
	 */
	private void createDir(String name) {
		File f = new File(name);
		f.mkdirs();
	}

	/**
	 * Removes the test directory
	 * 
	 * @param name
	 *            the directory name
	 */
	private void removeDir(String name) {
		File f = new File(name);
		f.delete();
	}
}
