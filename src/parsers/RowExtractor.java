package parsers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import oracle.kv.table.FieldDef;

/**
 * RowExtractors extracts, from an input stream, pieces of contents each of
 * which representing a complete kvstore table row.
 */
public interface RowExtractor {

	/**
	 * Reads the input stream and extract contents representing each table rows.
	 * 
	 * @param in
	 *            the input
	 * @throws IOException
	 *             on I/O error
	 */
	void extractRows(InputStream in) throws IOException;

	/**
	 * Only used when generating schema on sample data,
	 * 
	 * @param in
	 * @return Field names and types
	 */
	Map<String, FieldDef.Type> extractSchemas(InputStream in);
}
