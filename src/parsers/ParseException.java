package parsers;

/**
 * Represent an error condition when parsing rows.
 */
public class ParseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param msg
	 *            the cause of the error
	 */
	public ParseException(String msg) {
		super(msg);
	}
}
