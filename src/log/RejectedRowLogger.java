package log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class used for logging rejected rows.
 */
public class RejectedRowLogger {

	private static final Logger logger = LoggerFactory.getLogger("reject");

	/**
	 * Logs the rejected row.
	 * 
	 * @param originalText
	 *            the original text of the row.
	 * @param file
	 *            the file name
	 */
	public static void log(String originalText, String file) {
		logger.info(String.format("%s (%s)", originalText, file));
	}

}
