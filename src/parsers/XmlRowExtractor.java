package parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

import loader.Loader;
import log.RejectedRowLogger;
import oracle.kv.table.FieldDef.Type;
import data.ExtractedRow;
import data.TableDataFile;

/**
 * XmlRowExtractor is used to parse xml data files.
 * 
 * The tag name shall match the configured 'interested tag'. Child tag's
 * attributes will be aggregated to top interested tag.<br>
 * And if child tag has the same attribute name, the attribute value may
 * overwrite parent's attribute.
 * 
 */
public class XmlRowExtractor extends TextRowExtractor {

	private String xmlTagName;
	private TableDataFile file;
	// Buffer to store a record's original string
	private final StringBuffer recordBufferString = new StringBuffer(1000);
	// tag's key value pairs
	private HashMap<String, String> tempKeyValuePairs;

	/** Extractor's status fileds **/
	private boolean tagBegining = false;
	private boolean tagBegan = false;
	private boolean tagEnding = false;
	private boolean tagEnded = false;

	private boolean valueSingleQuoteBegin = false;
	private boolean valueDoubleQuoteBegin = false;

	private String tempAttrName;
	private String tempAttrValue;
	private Stack<String> tagNameStack = new Stack<String>();

	/**
	 * The converter used to convert rows extracted by this extractor.
	 */
	private MapRowConverter converter;

	public XmlRowExtractor(TableDataFile file, ArrayBlockingQueue<ExtractedRow> queue) {
		super(file, queue);
		this.file = file;
		this.xmlTagName = Loader.getConfig().getXmlRowTagName();
		if (xmlTagName == null || xmlTagName.length() == 0)
			xmlTagName = file.getTableFullName();
	}

	@Override
	protected void extractTextRows(BufferedReader r) throws IOException {
		converter = new MapRowConverter(file);
		String line = null;
		while ((line = r.readLine()) != null) {
			parseLine(line, true);
		}
	}

	private void parseLine(String line, boolean isAddingToQueue) {
		tempKeyValuePairs = new HashMap<String, String>();
		int ltindex = -1;
		int attrNameIndex = -1;
		int attrValueIndex = -1;
		for (int i = 0; i < line.length(); i++) {
			char currentChar = line.charAt(i);
			switch (currentChar) {
			case '<':
				ltindex = i;
				tagBegining = true;
				break;
			case '/':
				tagEnding = true;
				if (tagBegining)
					tagBegining = false;
				break;
			case '>':
				if (tagEnding) {
					tagEnding = false;
					tagEnded = true;
					if (isInterested(tagNameStack.peek())) {
						recordBufferString.append(currentChar);
						if (isAddingToQueue) {
							// finish one record
							addRow(tempKeyValuePairs, recordBufferString.toString());
						} else {
							aggregateTypes(tempKeyValuePairs);
						}
						tempKeyValuePairs = new HashMap<String, String>();
					}
				} else if (tagBegining) {
					tagBegining = false;
					tagBegan = true;
					tagNameStack.push(line.substring(ltindex + 1, i));
					tagEnded = false;
				}
				break;
			case '\'':
				if (!valueDoubleQuoteBegin) {
					valueSingleQuoteBegin = !valueSingleQuoteBegin;
					if (valueSingleQuoteBegin) {
						attrValueIndex = i;
					} else {
						tempAttrValue = line.substring(attrValueIndex + 1, i);
						tempKeyValuePairs.put(tempAttrName, tempAttrValue);
					}
				}
				break;
			case '\"':
				if (!valueSingleQuoteBegin) {
					valueDoubleQuoteBegin = !valueDoubleQuoteBegin;
					if (valueDoubleQuoteBegin) {
						attrValueIndex = i;
					} else {
						tempAttrValue = line.substring(attrValueIndex + 1, i);
						tempKeyValuePairs.put(tempAttrName, tempAttrValue);
					}
				}
				break;
			case '=':
				if (valueSingleQuoteBegin || valueDoubleQuoteBegin) {

				} else {
					tempAttrName = line.substring(attrNameIndex + 1, i);
				}
				break;
			case ' ':
				if (valueSingleQuoteBegin || valueDoubleQuoteBegin) {

				} else if (tagBegining) {
					attrNameIndex = i;
					tagBegining = false;
					tagBegan = true;
					tagNameStack.push(line.substring(ltindex + 1, i));
					tagEnding = false;
					tagEnded = false;

					// TODO save tagname
				} else {
					attrNameIndex = i;
					tagBegining = false;
					tagBegan = false;
					tagEnding = false;
					tagEnded = false;
				}
				break;
			case '?':
				if (tagBegining)
					tagBegining = false;
				break;
			default:
				break;
			}
			if (tagEnded) {
				recordBufferString.setLength(0);
				tagBegining = false;
				tagBegan = false;
				tagEnding = false;
				tagEnded = false;
				tagNameStack.pop();
			} else if (tagBegan && !isInterested(tagNameStack.peek())) {
				recordBufferString.setLength(0);
				tagBegining = false;
				tagBegan = false;
				tagEnding = false;
				tagEnded = false;
			} else {
				recordBufferString.append(currentChar);
			}
		}
	}

	/**
	 * Creates an extracted row for the line and add it to the queue.
	 * 
	 * @param keyValuePairs
	 *            the field names and values
	 * @param record
	 *            the line text
	 */
	private void addRow(HashMap<String, String> keyValuePairs, String record) {
		putRow(new ExtractedRow(keyValuePairs, record, getFileName(), converter));
	}

	/**
	 * Determine if the tag is interested, if not interested, there will be no
	 * actions for table.put
	 * 
	 * @param tagName
	 * @return
	 */
	private boolean isInterested(String tagName) {
		return xmlTagName.equals(tagName);
	}

	/**
	 * Only used for extractSchemas
	 */
	private Map<String, Type> types;

	/**
	 * Aggregate the types of fields by determine the types of the row values
	 * 
	 * @param keyvalues
	 */
	private void aggregateTypes(Map<String, String> keyvalues) {
		if (types == null)
			return;

		for (String key : keyvalues.keySet()) {
			// explore each key-value pairs to determine the type
			String value = keyvalues.get(key);
			Type currentValueType = Type.STRING;
			if (value == null) {
				// null value
			} else if (value.matches("true|false")) {
				currentValueType = Type.BOOLEAN;
			} else if (value.matches("[0-9]+")) {
				currentValueType = Type.INTEGER;
			} else if (value.matches("[0-9]+\\.[0-9]+")) {
				currentValueType = Type.DOUBLE;
			}
			Type type = types.get(key);
			if (type == null || currentValueType == Type.STRING) {
				types.put(key, currentValueType);
			} else if (type == Type.STRING) {
				types.put(key, Type.STRING);
			} else if (currentValueType == Type.DOUBLE) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
				else
					types.put(key, Type.DOUBLE);
			} else if (currentValueType == Type.INTEGER) {
				if (type == Type.BOOLEAN)
					types.put(key, Type.STRING);
			} else if (currentValueType == Type.BOOLEAN) {
				if (type == Type.INTEGER || type == Type.DOUBLE)
					types.put(key, Type.STRING);
			}
		}
	}

	@Override
	public Map<String, Type> extractSchemas(InputStream in) {
		types = new HashMap<String, Type>();
		String line = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {
			while ((line = reader.readLine()) != null) {
				parseLine(line, false);
			}
		} catch (IOException e) {
			RejectedRowLogger.log(line, getFileName());
		}
		return types;
	}

}
