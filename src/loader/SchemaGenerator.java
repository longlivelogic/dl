package loader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.kv.table.FieldDef.Type;
import parsers.ExtractorFactory;
import parsers.RowExtractor;
import data.TableDataFile;

public class SchemaGenerator {
	/**
	 * Fetch sample data from the data directories and generate schema scripts
	 * separately for the dat files
	 */
	public static void generateSchemas() {
		for (Set<File> directories : Loader.getConfig().getReaderSources()) {
			// explore each configured folder
			for (File directory : directories) {
				// explore each file
				for (File file : directory.listFiles()) {
					sampleOneFile(file);
				}
			}
		}
	}

	private static void sampleOneFile(File file) {
		String fileName = file.getName().toString();
		if (fileName.matches("^(.+)(\\.\\d+)?\\.(csv|xml|json)$")) {
			TableDataFile tableFile = new TableDataFile(file.toPath());
			RowExtractor extractor = ExtractorFactory.createExtractor(tableFile, null);
			// just read some sample data
			try (BufferedReader sampleDataReader = new BufferedReader(new FileReader(file));) {
				char[] charBuff = new char[1024 * 4];
				int readSize = sampleDataReader.read(charBuff);
				if (readSize > 0) {
					byte[] streamBytes = new String(charBuff).getBytes("utf-8");
					ByteArrayInputStream bis = new ByteArrayInputStream(streamBytes);
					// parse sample data
					Map<String, Type> keyTypes = extractor.extractSchemas(bis);
					if (keyTypes != null && keyTypes.size() > 0) {
						// generate schema file
						generateSchemaFile(keyTypes, file, tableFile.getTableFullName());
					}
				}
			} catch (IOException e) {
				System.err.println("Failed to read the file " + file.getAbsolutePath());
			}
		}
	}

	private static void generateSchemaFile(Map<String, Type> keyTypes, File file,
			String tableFullName) {
		StringBuffer tableScript = new StringBuffer("## Enter into table creation mode\r\n");
		tableScript.append("table create -name " + tableFullName + "\r\n");
		tableScript.append("## Now add the fields\r\n");
		// generate table fields
		System.out.println("Creating table script for " + tableFullName);
		int count = 0;
		List<String> fieldNames = new ArrayList<String>();
		for (String key : keyTypes.keySet()) {
			System.out.println("\t" + (++count) + "->" + key + "(" + keyTypes.get(key) + ")");
			tableScript.append("add-field -type " + keyTypes.get(key).toString() + " -name " + key
					+ "\r\n");
			fieldNames.add(key);
		}

		tableScript.append("## A primary key must be defined for every table\r\n");
		System.out.println("Please choose primary fields numbers (> [num1],[num2],[num3]...):");
		String line;
		String[] fieldNumStrings = null;
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		try {
			line = bfr.readLine();
			fieldNumStrings = line.split(",");
		} catch (IOException e1) {
			// ignore input error
		}
		List<Integer> fieldNums = new ArrayList<Integer>();
		if (fieldNumStrings != null && fieldNumStrings.length > 0) {
			for (String filedNumString : fieldNumStrings) {
				try {
					int fieldNum = Integer.parseInt(filedNumString);
					if (fieldNum > 0 && fieldNum <= fieldNames.size()) // input
																		// must
																		// be
																		// valid
						fieldNums.add(fieldNum);
				} catch (Exception e) {
					// ignore format error
					e.printStackTrace();
				}
			}
		}

		if (fieldNums.size() == 0) {
			System.out.println("Invalid input, use first field as primary key.");
			fieldNums.add(1);
		}

		tableScript.append("primary-key");
		String primaryKeys = "";
		for (int idx : fieldNums) {
			tableScript.append(" -field " + fieldNames.get(idx - 1));
			primaryKeys += "," + idx;
		}
		tableScript.append("\r\n");

		System.out.println("Please choose shard key in:" + primaryKeys.substring(1));
		int shardIdx = -1;
		try {
			line = bfr.readLine();
			shardIdx = Integer.parseInt(line);
		} catch (Exception e1) {
			// ignore input error
			e1.printStackTrace();
		}

		if (shardIdx == -1) {
			System.out.println("Invalid input, use first primary key as shard key.");
			shardIdx = fieldNums.get(0);
		}

		tableScript.append("shard-key -field " + fieldNames.get(shardIdx - 1) + "\r\n");
		tableScript.append("## Exit table creation mode\r\n");
		tableScript.append("exit\r\n");
		tableScript.append("plan add-table -name " + tableFullName + " -wait\r\n");

		try {
			bfr.close();
		} catch (IOException e) {
			// ignore the error on closing
		}
		System.out.println(tableScript.toString());
		File tableScriptFile = new File(file.toPath() + ".table_define.txt");
		FileWriter fw = null;
		try {
			if (!tableScriptFile.exists())
				tableScriptFile.createNewFile();
			fw = new FileWriter(tableScriptFile);
			fw.write(tableScript.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null)
				try {
					fw.close();
				} catch (IOException e) {
				}
		}
	}
}
