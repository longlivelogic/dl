/**
 * 
 */
package parsers;

import kvstore.TableRowHelper;
import data.TableDataFile;

/**
 * The base class for all RowConverters.
 */
public abstract class AbstractRowConverter implements RowConverter {

	/**
	 * The TableRowHelper which can be used to create KVStore rows.
	 */
	private final TableRowHelper rowHelper;

	/**
	 * Constructor.
	 * 
	 * @param rowHelper
	 *            the row helper
	 */
	public AbstractRowConverter(TableDataFile file) {
		this.rowHelper = new TableRowHelper(file.getTable());
	}

	/**
	 * @return the rowHelper
	 */
	protected final TableRowHelper getRowHelper() {
		return rowHelper;
	}

}
