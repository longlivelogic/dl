package util;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * A helper class providing easy-to-use methods for working with
 * ArrayBlockingQueues.
 */
public class QueueHelper {

	/**
	 * Puts an object into the queue. Retry if interrupted.
	 * 
	 * @param obj
	 *            the object to be put
	 * @param queue
	 *            the queue
	 */
	public static <T> void putWithRetry(T obj, ArrayBlockingQueue<T> queue) {
		boolean queued = false;
		while (!queued) {
			try {
				queue.put(obj);
				queued = true;
			} catch (InterruptedException e) {
				// retry
			}
		}
	}

	/**
	 * Takes an element from the queue. Retry if interrupted.
	 * 
	 * @param queue
	 *            the queue
	 * @return an element
	 */
	public static <T> T takeWithRetry(ArrayBlockingQueue<T> queue) {
		while (true) {
			try {
				return queue.take();
			} catch (InterruptedException e) {
				// retry
			}
		}
	}

}
