package workers;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import loader.Loader;
import oracle.kv.table.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.ExtractedRow;
import data.TableDataFile;

/**
 * The DataFileReader is used to read a list of data files for further
 * processing. The files are processed in ascending order of their table names
 * if they are in the same directory.
 * 
 * NOTE:
 * this ordering makes sense for child tables also. if we enforce this ordering
 * and assume that table_name = filename, then the parent table is always loaded
 * before child tables if they are in the same directory. we can support child
 * tables for free.
 * 
 * DataFileReader also performs necessary pre-processing before opening each
 * file. For example:
 * 1) determine the table to which the data should be loaded
 * 2) verify the existence of the table and get the table handle
 * 3) determine the type of the file and creates the corresponding row extractor
 * 
 * Multi-threading:
 * Designed to be used by single thread only.
 * 
 * To be tested:
 * a) the current design is to run the extractor in a separate thread. this
 * thread is i/o intensive while the extractor is cpu/memory intensive.
 * separating these two tasks may improve throughput, but we need to
 * verify this. in this design the two threads are connected with piped
 * streams.
 */
public class DataFileReader implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(DataFileReader.class);

	/**
	 * The buffer size of the piped stream.
	 */
	private static final int PIPE_BUFFER_SIZE = 4 * 1024 * 1024;

	/**
	 * The id for this reader.
	 */
	private final int readerId;

	/**
	 * The list of directories to be processed.
	 */
	private final Set<File> srcDirs;

	/**
	 * The queue used to stage extracted rows for this reader.
	 */
	private final ArrayBlockingQueue<ExtractedRow> queue;

	/**
	 * Constructor.
	 * 
	 * @param dirs
	 *            the set of directories to be processed
	 * @param queue
	 *            the queue used to stage rows read by this reader
	 * @param the
	 *            reader id
	 */
	public DataFileReader(Set<File> dirs, ArrayBlockingQueue<ExtractedRow> queue, int id) {
		this.srcDirs = dirs;
		this.queue = queue;
		this.readerId = id;
	}

	@Override
	public void run() {
		try {
			for (File dir : srcDirs) {
				List<TableDataFile> files = listValidDataFiles(dir);
				Collections.sort(files);

				for (TableDataFile file : files) {
					readFile(file);
				}
			}
		} catch (Exception e) {
			logger.error(String.format("Fatal error happened for reader '%d'", readerId), e);
		}
	}

	/**
	 * Lists all the direct child regular files under the given directory,
	 * skipping files for which we cannot retrieve its table.
	 * 
	 * @param dir
	 *            the directory to be listed
	 * @return direct child regular files as TableDataFiles
	 */
	private List<TableDataFile> listValidDataFiles(File dir) {
		List<TableDataFile> files = new ArrayList<>();
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(dir.toPath())) {
			for (Path path : ds) {
				addPathToListIfValid(path, files);
			}
		} catch (IOException | DirectoryIteratorException e) {
			logger.error("Cannot list files under: '" + dir + "'.", e);
		}
		return files;
	}

	/**
	 * If the path refers to a valid table data file, converts it to a
	 * TableDataFile and adds it to the list. A path refers to a valid file if
	 * it is a regular file and we can determine the table name for the file.
	 * 
	 * @param path
	 *            the file path
	 * @param list
	 *            the result list
	 */
	private void addPathToListIfValid(Path path, List<TableDataFile> list) {
		try {
			if (Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS)) {
				list.add(new TableDataFile(path));
			}
		} catch (IllegalArgumentException e) {
			logger.error(
					String.format("Failed to determine the table name. Skip file: '%s'",
							path.toString()), e);
		}
	}

	/**
	 * Reads the data file and sends its content to an extractor for processing.
	 * 
	 * @param file
	 *            the file to read
	 */
	private void readFile(TableDataFile file) {
		if (isTableValid(file)) {
			startReading(file);
		}
	}

	/**
	 * Checks if we can retrieve the table handle for the file.
	 * 
	 * @param file
	 *            the data file
	 * @return true if the table handle is valid
	 */
	private boolean isTableValid(TableDataFile file) {
		Table t = file.getTable();
		if (t == null) {
			logger.error(String.format("Table '%s' does not exist. Skip file: '%s'.",
					file.getTableFullName(), file.getFileName()));
			return false;
		}
		return true;
	}

	/**
	 * Opens a pipe and starts an extractor worker for the given data file.
	 * Returns the output side of the pipe, which is used to channel data to the
	 * extractor.
	 * 
	 * @param file
	 *            the data file
	 * @return the output pipe stream
	 * @throws IOException
	 *             if the pipe cannot be opened
	 */
	private PipedOutputStream openPipeAndStartExtractor(TableDataFile file) throws IOException {
		PipedInputStream sink = new PipedInputStream(PIPE_BUFFER_SIZE);
		PipedOutputStream source = new PipedOutputStream(sink);

		Loader.getThreadPools().submit(new ExtractorWorker(file, sink, queue));

		return source;
	}

	/**
	 * Starts and extractor and reads the content.
	 * 
	 * @param file
	 *            the data file
	 */
	private void startReading(TableDataFile file) {
		try (OutputStream source = openPipeAndStartExtractor(file)) {
			Files.copy(file.getFile(), source);
		} catch (IOException e) {
			logger.error(String.format("Failed to read '%s'.", file.toString()), e);
		}
	}

}
