package loader;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link LoaderConfig} handles global configurations for the entire Loader
 * application.
 * 
 * Multi-threading:
 * Designed to be read-only, and thus is effectively thread-safe.
 */
public class LoaderConfig {

	private static final Logger logger = LoggerFactory.getLogger(LoaderConfig.class);

	/*
	 * Keys for various configure options. Allow package access for the ease of
	 * tests.
	 */
	static final String STORENAME_KEY = "storeName";
	static final String HOSTS_KEY = "hosts";
	static final String DISK_SRC_KEY = "reader.%d.src";
	static final String WRITER_COUNT_KEY = "writerCount";
	static final String WRITER_BATCH_SIZE_KEY = "writerBatchSize";
	static final String XML_ROW_TAG_NAME_KEY = "xml.rowTagName";
	static final String AUTO_FILL_PRIMAY_KEY = "autoFillPrimaryKey";

	/**
	 * The KVStore name.
	 */
	private final String storeName;

	/**
	 * The KVStore helper hosts.
	 */
	private final String[] hosts;

	/**
	 * The sets of source file directories to be processed for each reader.
	 */
	private final Set<File>[] readerSources;

	/**
	 * The number of KVStore row writers.
	 */
	private final int writerCount;

	/**
	 * The maximum number of rows that may go into one batch operation.
	 */
	private final int batchSize;

	/**
	 * The tag name of XML row elements.
	 */
	private final String xmlRowTagName;

	/**
	 * The flag of whether to fill primary key or cancel inserting the row
	 */
	private final boolean autoFillPrimaryKey;

	/**
	 * Constructs a configuration from properties.
	 * 
	 * @param p
	 *            the properties
	 */
	public LoaderConfig(Properties p) {
		this.storeName = getStoreName(p);
		this.hosts = getHosts(p);
		this.readerSources = getSources(p);
		this.writerCount = getWriterCount(p);
		this.batchSize = getBatchSize(p);
		this.xmlRowTagName = getXmlRowTagName(p);
		this.autoFillPrimaryKey = getAutoFillPrimaryKey(p);
	}

	/**
	 * Gets the KVStore name.
	 * 
	 * @param p
	 *            the properties
	 * @return the store name
	 */
	private String getStoreName(Properties p) {
		return getStringProperty(p, STORENAME_KEY);
	}

	/**
	 * Gets the helper hosts.
	 * 
	 * @param p
	 *            the properties
	 * @return the helper hosts
	 */
	private String[] getHosts(Properties p) {
		String hostProp = getStringProperty(p, HOSTS_KEY);

		String[] hosts = hostProp.trim().split("\\s*,\\s*");
		Pattern pat = Pattern.compile("^.*:(\\d{1,5})$");
		for (String h : hosts) {
			Matcher m = pat.matcher(h);
			if (!m.matches() || Integer.parseInt(m.group(1)) > 65535) {
				throw new IllegalArgumentException("invalid host '" + h + "'.");
			}
		}

		return hosts;
	}

	/**
	 * Gets the source directories for each reader.
	 * 
	 * @param p
	 *            the properties
	 * @return the array of sets of directories, one for each reader
	 */
	private Set<File>[] getSources(Properties p) {
		List<String> srcProperties = collectSourceProperties(p);
		@SuppressWarnings("unchecked")
		Set<File>[] readerSrcs = new Set[srcProperties.size()];

		Set<File> all = new HashSet<>();
		for (int i = 0; i < readerSrcs.length; i++) {
			Set<File> sourceDirs = parseSourceProperties(srcProperties.get(i));
			sourceDirs.removeAll(all); // remove duplicates
			readerSrcs[i] = Collections.unmodifiableSet(sourceDirs);
			all.addAll(sourceDirs);
		}

		return readerSrcs;
	}

	/**
	 * Collects all source directory properties, stopping at the first disk
	 * number that does not specify source directories.
	 * 
	 * @param p
	 *            the properties
	 * @return a list of source directory properties, one for each disk
	 */
	private List<String> collectSourceProperties(Properties p) {
		List<String> srcProperties = new LinkedList<>();

		for (int i = 1;; i++) {
			String key = String.format(DISK_SRC_KEY, i);
			String prop = p.getProperty(key);
			if (prop != null) {
				srcProperties.add(prop);
			} else {
				return srcProperties;
			}
		}
	}

	/**
	 * Parses the source-directories property into a set of Files. Non-existent
	 * or non-directory files are removed and files are returned in canonical
	 * forms.
	 * 
	 * @param property
	 *            the property string
	 * @return a set of Files
	 */
	private Set<File> parseSourceProperties(String property) {
		String[] paths = property.trim().split("\\s*,\\s*");
		Set<File> dirs = new HashSet<>(paths.length);
		for (String p : paths) {
			File f = new File(p);
			if (f.exists() && f.isDirectory()) {
				try {
					dirs.add(f.getCanonicalFile());
				} catch (IOException e) {
					logger.error("Cannot canonicalize path: '" + p + "'. skipped.", e);
				}
			} else {
				logger.warn("'" + p + "' is not a directory. skipped.");
			}
		}
		return dirs;
	}

	/**
	 * Gets the writer count.
	 * 
	 * @param p
	 *            the properties
	 * @return the number of KVStore row writers specified
	 */
	private int getWriterCount(Properties p) {
		return getIntProperty(p, WRITER_COUNT_KEY);
	}

	/**
	 * Gets the writer batch size.
	 * 
	 * @param p
	 *            the properties
	 * @return the number of KVStore rows that may go into one batch operation
	 */
	private int getBatchSize(Properties p) {
		return getIntProperty(p, WRITER_BATCH_SIZE_KEY);
	}

	/**
	 * Gets the tag name of XML row elements.
	 * 
	 * @param p
	 *            the properties
	 * @return the XML row elements' tag name
	 */
	private String getXmlRowTagName(Properties p) {
		return getStringProperty(p, XML_ROW_TAG_NAME_KEY);
	}

	/**
	 * Get the tag of whether to auto fill the row's primay keys
	 * 
	 * @param p
	 * @return
	 */
	private boolean getAutoFillPrimaryKey(Properties p) {
		return "true".equalsIgnoreCase(p.getProperty(AUTO_FILL_PRIMAY_KEY, "false"));
	}

	/**
	 * Gets a string property.
	 * 
	 * @param p
	 *            the properties
	 * @param propName
	 *            the property name
	 * @return the string property value
	 */
	private String getStringProperty(Properties p, String propName) {
		String prop = p.getProperty(propName);
		if (prop == null || prop.trim().isEmpty()) {
			throw new IllegalArgumentException(propName + " cannot be empty.");
		} else {
			return prop.trim();
		}
	}

	/**
	 * Gets an integer property.
	 * 
	 * @param p
	 *            the properties
	 * @param propName
	 *            the property name
	 * @return the integer property value
	 */
	private int getIntProperty(Properties p, String propName) {
		try {
			return Integer.parseInt(p.getProperty(propName));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid parameter: " + propName, e);
		}
	}

	/**
	 * @return the KVStore storeName
	 */
	public String getStoreName() {
		return storeName;
	}

	/**
	 * @return the helper hosts
	 */
	public String[] getHosts() {
		return hosts;
	}

	/**
	 * @return the source-directories on readers
	 */
	public Set<File>[] getReaderSources() {
		return readerSources;
	}

	/**
	 * @return the number of readers configured
	 */
	public int getReaderCount() {
		return readerSources.length;
	}

	/**
	 * @return the writerCount
	 */
	public int getWriterCount() {
		return writerCount;
	}

	/**
	 * @return the batch size
	 */
	public int getBatchSize() {
		return batchSize;
	}

	/**
	 * @return the interested tag name in xml file that will be recognized as a
	 *         row
	 */
	public String getXmlRowTagName() {
		return xmlRowTagName;
	}

	public boolean isAutoFillPrimaryKey() {
		return autoFillPrimaryKey;
	}
}
