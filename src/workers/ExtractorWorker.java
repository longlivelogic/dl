package workers;

import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parsers.ExtractorFactory;
import parsers.RowExtractor;
import data.ExtractedRow;
import data.TableDataFile;

/**
 * The ParserWorker is used to run a parser to parse inputs into rows. This
 * serves as the provider of rows.
 * 
 * This is a thin wrapper around a parser instance.
 * 
 * Multi-threading: Designed to be used by single thread only.
 */
public class ExtractorWorker implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ExtractorWorker.class);

	/**
	 * The data file to be parsed.
	 */
	private final TableDataFile file;

	/**
	 * The input stream.
	 */
	private final InputStream input;

	/**
	 * The queue used to stage extracted rows.
	 */
	private final ArrayBlockingQueue<ExtractedRow> queue;

	/**
	 * Constructor.
	 * 
	 * @param file
	 *            the data file
	 * @param input
	 *            the input stream to be parsed
	 * @param queue
	 *            the queue to stage rows
	 */
	public ExtractorWorker(TableDataFile dataFile, InputStream input,
			ArrayBlockingQueue<ExtractedRow> queue) {
		this.file = dataFile;
		this.input = input;
		this.queue = queue;
	}

	@Override
	public void run() {
		try {
			RowExtractor extractor = ExtractorFactory.createExtractor(file, queue);
			extractor.extractRows(input);
		} catch (Exception e) {
			logger.error(String.format("Failed to parse file '%s'", file.getFileName()), e);
		}
	}

}
