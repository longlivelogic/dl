package parsers;

import oracle.kv.table.Row;
import oracle.kv.table.Table;

/**
 * The JsonRowConverter directly invokes table.createRowFromJson() to transform
 * string record to row.
 * 
 * @author wlu
 *
 */
public class JsonRowConverter implements RowConverter {
	private final Table table;

	/**
	 * Constructor to pass the corresponding table that the json file shall
	 * match
	 * 
	 * @param table
	 */
	public JsonRowConverter(Table table) {
		this.table = table;
	}

	@Override
	public Row convert(Object rowContent) throws ParseException {
		if (table != null) {
			Row row = table.createRowFromJson(rowContent.toString(), true);
			return row;
		} else
			return null;
	}

}
