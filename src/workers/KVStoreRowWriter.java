package workers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ArrayBlockingQueue;

import kvstore.KVStoreHelper;
import kvstore.TableRowHelper;
import log.RejectedRowLogger;
import oracle.kv.table.Row;
import oracle.kv.table.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parsers.ParseException;
import util.QueueHelper;
import data.ExtractedRow;
import data.RowShardKey;

/**
 * KVStoreRowWriter is used to import KVStore rows into the underlying
 * kvstore. This serves as the consumer of the rows.
 *
 */
public class KVStoreRowWriter implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(KVStoreRowWriter.class);

	/**
	 * The maximum number of rows buffered in total.
	 */
	private static final int MAX_BUFFERED_ROW = 20000;

	/**
	 * The queue from which extracted rows are consumed.
	 */
	private final ArrayBlockingQueue<ExtractedRow> queue;

	/**
	 * The maximum number of rows that can be put in one batch.
	 */
	private final int batchSize;

	/**
	 * TableRowHelper cache for data files.
	 */
	private WeakHashMap<String, TableRowHelper> rowHelperCache;

	/**
	 * The buffer used to buffer rows for batch insertion.
	 */
	private Map<RowShardKey, List<ExtractedRow>> batchBuffer;

	/**
	 * The number of rows buffered in batchBuffer;
	 */
	private int bufferedRowCount;

	/**
	 * @param queue
	 *            the queue to retrived extracted rows
	 * @param batchSize
	 *            the batch size
	 */
	public KVStoreRowWriter(ArrayBlockingQueue<ExtractedRow> queue, int batchSize) {
		this.queue = queue;
		this.batchSize = batchSize;
		rowHelperCache = new WeakHashMap<>();
		batchBuffer = new HashMap<RowShardKey, List<ExtractedRow>>();
		bufferedRowCount = 0;
	}

	@Override
	public void run() {
		try {
			ExtractedRow row;
			while ((row = QueueHelper.takeWithRetry(queue)) != ExtractedRow.END_OF_QUEUE) {
				processRow(row);
			}
			flushAll();
		} catch (Exception e) {
			logger.error(String.format("Fatal error happened for writer"), e);
		}
	}

	/**
	 * Processes the given extracted row.
	 * 
	 * @param extractedRow
	 *            the row
	 */
	private void processRow(ExtractedRow extractedRow) {
		try {
			RowShardKey shardKey = getShardKey(extractedRow);
			putRowInBatch(shardKey, extractedRow);
		} catch (ParseException e) {
			RejectedRowLogger.log(extractedRow.getOriginalText(), extractedRow.getFileName());
		}
	}

	/**
	 * Gets the shard key for the given row.
	 * 
	 * @param extractedRow
	 *            the row
	 * @return the shard key field values
	 * @throws ParseException
	 *             on parse error
	 */
	private RowShardKey getShardKey(ExtractedRow extractedRow) throws ParseException {
		Row kvRow = extractedRow.convertToKVStoreRow();
		TableRowHelper rowHelper = getTableRowHelper(extractedRow.getFileName(), kvRow.getTable());
		return rowHelper.getShardKey(kvRow);
	}

	/**
	 * Returns the TableRowHelper for the file, creates one if necessary.
	 * 
	 * @param fileName
	 *            file name
	 * @param table
	 *            the table for the TableRowHelper
	 * @return a table row helper
	 */
	private TableRowHelper getTableRowHelper(String fileName, Table table) {
		TableRowHelper rowHelper = rowHelperCache.get(fileName);
		if (rowHelper == null) {
			rowHelper = new TableRowHelper(table);
			rowHelperCache.put(fileName, rowHelper);
		}
		return rowHelper;
	}

	/**
	 * Puts the row in the batch buffer, and flushes row if necessary.
	 * 
	 * @param shardKey
	 *            the
	 * @param kvRow
	 */
	private void putRowInBatch(RowShardKey shardKey, ExtractedRow extractedRow) {
		if (shardKey == null) {
			flushRow(extractedRow);
			return;
		}

		List<ExtractedRow> batchedRow = getRowBatch(shardKey);
		batchedRow.add(extractedRow);
		bufferedRowCount++;

		if (batchedRow.size() >= batchSize) {
			flushBatch(batchedRow);
			return;
		}

		if (bufferedRowCount > MAX_BUFFERED_ROW) {
			flushAll();
		}
	}

	/**
	 * Gets the list of rows buffered under the given shard key.
	 * 
	 * @param shardKey
	 *            the shard key
	 * @return a list of buffered rows
	 */
	private List<ExtractedRow> getRowBatch(RowShardKey shardKey) {
		List<ExtractedRow> batchedRow = batchBuffer.get(shardKey);
		if (batchedRow == null) {
			batchedRow = new LinkedList<>();
			batchBuffer.put(shardKey, batchedRow);
		}
		return batchedRow;
	}

	/**
	 * Flushes a single row.
	 * 
	 * @param row
	 *            the row
	 */
	private void flushRow(ExtractedRow row) {
		try {
			Row kvrow = row.convertToKVStoreRow();
			if (kvrow != null)
				KVStoreHelper.getInstance().putRow(kvrow);
			else
				RejectedRowLogger.log(row.getOriginalText(), row.getFileName());
		} catch (Exception e) {
			RejectedRowLogger.log(row.getOriginalText(), row.getFileName());
		}
	}

	/**
	 * Flushes a batch of rows with the same shard key.
	 * 
	 * @param batchedRow
	 *            a list of rows
	 */
	private void flushBatch(List<ExtractedRow> batchedRow) {
		List<Row> rows = new ArrayList<>(batchedRow.size());
		Row kvrow = null;
		for (ExtractedRow r : batchedRow) {
			kvrow = r.convertToKVStoreRow();
			if (kvrow != null)
				rows.add(kvrow);
		}
		List<Integer> failed = KVStoreHelper.getInstance().putRows(rows);

		for (Integer index : failed) {
			ExtractedRow r = batchedRow.get(index);
			RejectedRowLogger.log(r.getOriginalText(), r.getFileName());
		}

		bufferedRowCount -= batchedRow.size();
		batchedRow.clear();
	}

	/**
	 * Flushes all rows in the batch buffer.
	 */
	private void flushAll() {
		for (List<ExtractedRow> batch : batchBuffer.values()) {
			if (batch.size() == 1) {
				flushRow(batch.get(0));
				bufferedRowCount--;
			} else {
				flushBatch(batch);
			}
		}
	}
}
